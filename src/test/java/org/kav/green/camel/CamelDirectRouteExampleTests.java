package org.kav.green.camel;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.apache.camel.ProducerTemplate;

@SpringBootTest
class CamelDirectRouteExampleTests {

	@Autowired
	private ProducerTemplate producerTemplate;

	@Test
	void contextLoads() {
		this.producerTemplate.sendBody("direct:processCount", "Test1");
		this.producerTemplate.sendBody("direct:processCount", "Test2");
		this.producerTemplate.sendBody("direct:processCount", "Test3");
		this.producerTemplate.sendBody("direct:processCount", "Test4");
		this.producerTemplate.sendBody("direct:processCount", "Test5");
		this.producerTemplate.sendBody("direct:processCount", "Test6");
		this.producerTemplate.sendBody("direct:processCount", "Test7");
	}
}
