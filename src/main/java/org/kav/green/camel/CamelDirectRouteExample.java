package org.kav.green.camel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelDirectRouteExample {

	public static void main(String[] args) {
		SpringApplication.run(CamelDirectRouteExample.class, args);
	}

}
