package org.kav.green.camel.route.builder;

//import org.apache.camel.ProducerTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.kav.green.camel.route.processor.CheckCountProcessor;
import org.kav.green.camel.util.ProcessCount;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class DirectRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        ProcessCount processCount = new ProcessCount();
        from("direct:processCount")
                .process(new CheckCountProcessor(processCount));
    }
}
