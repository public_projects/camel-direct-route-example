package org.kav.green.camel.route.processor;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.kav.green.camel.util.ProcessCount;

@Slf4j
public class CheckCountProcessor implements Processor {
    private ProcessCount processCount;
    private int limit = 5;

    public CheckCountProcessor(ProcessCount processCount){
        this.processCount = processCount;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        String name = (String) exchange.getIn().getBody();
        int count = processCount.get(name);

        if (count < limit) {
            log.info("Processed {}", name);
            processCount.incrementAndGet(name);
            log.info("Increase {} to {}.", name, processCount.get(name));
        } else {
            log.info("Limit exceeded! {}", count);
        }
    }
}
