package org.kav.green.camel.util;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ThreadLocalRandom;

@Slf4j
public final class ProcessCount {
    public int count = 0;

    public void incrementAndGet(String name) {
        sleep("incrementAndGet", name);
        count++;
    }

    public int decrementAndGet(String name) {
        sleep("decrementAndGet", name);
        count--;
        return count;
    }

    public int get(String name) {
        sleep("get", name);
        return count;
    }

    private static void sleep(String method, String name) {
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        try {
            int sleepDuration = threadLocalRandom.nextInt(5000);
            log.info("Sleep [{}] on [{}] for [{}]milis", method, name, sleepDuration);
            Thread.sleep(threadLocalRandom.nextInt(sleepDuration));
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
